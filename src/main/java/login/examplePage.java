package login;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

public class examplePage extends BasePage {
	public examplePage(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}
	/** Example : Click on button JOIN NOW */
	public void clickButtonJoinNow() {
		driver.findElement(By.id("ovo.id.stage:id/btn_join")).click();
	}

}

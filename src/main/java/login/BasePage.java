package login;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public abstract class BasePage {
	protected AndroidDriver<AndroidElement> driver;
	protected int waitingTimeSecond = 2;
	protected int waitingTimeMiliSecond = 250;

	public BasePage(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void waitProgress() {
		driver.manage().timeouts().implicitlyWait(waitingTimeMiliSecond, TimeUnit.MILLISECONDS);
		int i = 0;
		while (!driver.findElements(By.className("android.widget.ProgressBar")).isEmpty() && i < 50) {
			try {
				Thread.sleep(waitingTimeMiliSecond);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i = i++;
		}
		assertTrue(i < 50, "Timeout.");
		driver.manage().timeouts().implicitlyWait(waitingTimeSecond, TimeUnit.SECONDS);
	}
}
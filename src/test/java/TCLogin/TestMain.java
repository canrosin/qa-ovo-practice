package TCLogin;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public abstract class TestMain {

	protected static AppiumDriverLocalService service;
	protected static AndroidDriver<AndroidElement> driver;

	@BeforeSuite
	protected void startAppium() {
		service = launchAppium("0.0.0.0");
	}

	/**This is for setup the capabilities to run the launch the app*/
	@BeforeTest
	protected void setUp(){
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "Android Smartphone");
		capabilities.setCapability("udid", "emulator-5554");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appPackage", "ovo.id.stage");
		capabilities.setCapability("appActivity", "ovo.id.loyalty.activity.LandingActivity");
		capabilities.setCapability("app", new File("OVO-STAGING_2.1.1.apk").getAbsolutePath());
		capabilities.setCapability("autoGrantPermissions", "true");
		capabilities.setCapability("newCommandTimeout", 60);
		capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.APPIUM);
		try {
			driver = new AndroidDriver<>(new URL(service.getUrl().toString()), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

	}

	@AfterSuite
	public void stopAppium() {
		killAppium(service);

	}
	/**This is for start the local appium server from Commanprompt*/
	public static AppiumDriverLocalService launchAppium(
			String ip
	) {
		AppiumServiceBuilder builder = new AppiumServiceBuilder().withIPAddress(ip).usingAnyFreePort()
				.withArgument(GeneralServerFlag.SESSION_OVERRIDE).withArgument(GeneralServerFlag.LOG_LEVEL, "error");
		AppiumDriverLocalService appiumDriverLocalService = AppiumDriverLocalService.buildService(builder);
		appiumDriverLocalService.start();
		return appiumDriverLocalService;
	}
	/**This is for stop the local appium server from Commanprompt*/
	public static void killAppium(AppiumDriverLocalService appiumDriverLocalService) {
		appiumDriverLocalService.stop();
	}

}
